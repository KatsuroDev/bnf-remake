extends Camera2D

export(NodePath) var target
onready var target_object = get_node(target)
export var acceleration_x = 1
export var acceleration_y = 1

var shake_amount = Vector2.ZERO
var shake_offset = Vector2.ZERO
var shake_status_quo_speed = 0.0

func _ready():
	GameEvents.connect("trunk_on_floor", self, "shake")
	
func reset():
	target = null
	target_object = null
	position.x = 528
	position.y = 560
	
func set_new_target(new_target):
	target_object = new_target

func _physics_process(delta):
	#position = player.position
	if(target_object != null):
		self.position.x += (target_object.get_global_transform().get_origin().x - self.position.x) * acceleration_x * delta
		self.position.y += (target_object.get_global_transform().get_origin().y  - self.position.y) * acceleration_y * delta
	shake_process(delta)

func shake(amount, status_quo_speed):
	shake_amount = amount
	shake_status_quo_speed = status_quo_speed

func shake_process(delta):
	shake_amount.x += -shake_amount.x * shake_status_quo_speed * delta
	shake_amount.y += -shake_amount.y * shake_status_quo_speed * delta
	
	
	shake_offset = Vector2(rand_range(-shake_amount.x, shake_amount.x), rand_range(-shake_amount.y, shake_amount.y))
	
	self.position.x += shake_offset.x
	self.position.y += shake_offset.y
