extends Node

signal player_entered_trunk
signal trunk_cut
signal trunk_on_floor
signal trunk_animation_finished
signal player_wins

const Difficulty = {EASY = 150, NORMAL = 135, HARD = 105, IMPOSSIBLE = 75}
