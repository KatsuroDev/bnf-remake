extends Area2D
signal wood_log_contact

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_BridgeTrigger_body_entered(body):
	if body.is_in_group("wood_log"):
		body.queue_free()
		emit_signal("wood_log_contact", self.position)
