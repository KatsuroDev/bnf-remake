extends Control

signal sign_text
signal start_game

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var menu_arr = ["Before Nightfall", "Cut Trees With\nSpace Bar\nTo Get Logs", "Get Logs To\nThe Sign\nTo Get To\nThe Fire\nBefore Night Falls"]
onready var menu_label = $Label
onready var difficulty_menu = $CenterContainer 
var menu_state = 0

var on_screen = true

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if(on_screen):
		if Input.is_action_just_pressed("ui_accept"):
			if(menu_state < menu_arr.size()):
				menu_state = menu_state + 1
		if(menu_state <= menu_arr.size()-1):
			menu_label.text = menu_arr[menu_state]
		if(menu_state == menu_arr.size()):
			menu_label.hide()
			difficulty_menu.show()
		if(menu_label.text == menu_arr[2]):
			emit_signal("sign_text")

func start(difficulty):
	yield(get_tree().create_timer(0.2), "timeout")
	emit_signal("start_game", difficulty)

func reset():
	menu_state = menu_arr.size()

func stop():
	on_screen = false
	self.hide()
