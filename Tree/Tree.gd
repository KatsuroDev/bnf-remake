
extends StaticBody2D
export var is_alive = true

var is_player_in = false

onready var trunk = $Position2D/Trunk
onready var trunk_animation = $Position2D/Trunk/AnimationPlayer


func _ready():
	if is_alive:
		trunk.show()
	else:
		trunk.hide()
	GameEvents.connect("trunk_cut", self, "_on_cut")

func _process(delta):
	if is_player_in and is_alive:
		GameEvents.emit_signal("player_entered_trunk", self)


func _on_Trigger_body_entered(body):
	if body is KinematicBody2D:
		is_player_in = true
		

func _on_Trigger_body_exited(body):
	if body is KinematicBody2D:
		is_player_in = false
		

func _on_cut(tree):
	if tree == self:
		is_alive = false
		trunk_animation.play("Timber")
		
func reset():
	is_alive = true
	trunk_animation.seek(0, true)
	trunk.show()




