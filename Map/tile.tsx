<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.2" name="tile" tilewidth="96" tileheight="96" tilecount="2" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="32" height="32" source="Sheets/tile.png"/>
 </tile>
 <tile id="7">
  <image width="96" height="96" source="GuttyKreumNatureTilesvol1/chopped96x96.png"/>
  <objectgroup draworder="index" id="2">
   <object id="5" x="27" y="62" width="37" height="27"/>
  </objectgroup>
 </tile>
</tileset>
