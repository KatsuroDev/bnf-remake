extends Node2D

export var max_tile = 5
export var tiles = 0
export(PackedScene) var BridgeTile

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func reset():
	tiles = 0
	# for every bridgeTile .queuefree
	for node in get_children():
		if node.filename.ends_with("BridgteTile.tscn"):
			node.queue_free()
	$BridgeTrigger.position.y = 380
	$BridgeTrigger/StaticBody2D.collision_mask = 1
	$BridgeTrigger/StaticBody2D.collision_layer = 1
	
	

func _on_BridgeTrigger_wood_log_contact(trigger_position):
	if tiles < max_tile:
		tiles += 1
		var bridge_tile = BridgeTile.instance()
		add_child(bridge_tile)
		bridge_tile.position = Vector2(trigger_position.x, trigger_position.y-12)
		$BridgeTrigger.position.y -= 32
		if tiles == max_tile:
			$BridgeTrigger/StaticBody2D.collision_mask = 0
			$BridgeTrigger/StaticBody2D.collision_layer = 0
