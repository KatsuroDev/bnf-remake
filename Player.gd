extends KinematicBody2D

export var walk_speed = 100
export var sprint_speed = 200
export var acceleration = 10
var inertia = 0.2
var tree_to_cut = null
var is_alive = false


enum MovementState { IDLE, MOVING }

var state = MovementState.IDLE
var velocity = Vector2()
var direction = Vector2()


func _ready():
	GameEvents.connect("player_entered_trunk", self, "_on_trunk_enter")
	$Sprite/AnimationPlayer.stop()
	pass

#
#func _process(_delta):
#	pass	


func player_cutting():
	GameEvents.emit_signal("trunk_cut", tree_to_cut)

func _physics_process(delta):
	if is_alive:
		direction = directionProcessWithInput(direction)
	else:
		direction = Vector2.ZERO
	var is_running = Input.is_action_pressed("game_running")
	
	if state == MovementState.IDLE:
		velocity.x += (0 - velocity.x) * acceleration * delta
		velocity.y += (0 - velocity.y) * acceleration * delta
	if state == MovementState.MOVING:
		$Sprite.flip_h = direction.x < 0
		velocity.x += (direction.x * (sprint_speed if is_running else walk_speed)-velocity.x) * acceleration * delta
		velocity.y += (direction.y * (sprint_speed if is_running else walk_speed)-velocity.y) * acceleration * delta
	
	
	velocity = move_and_slide(velocity, Vector2.ZERO, false, 4, PI/4, false)
	for index in get_slide_count():
		var collision = get_slide_collision(index)
		if collision.collider.is_in_group("Pushable"):
			collision.collider.apply_central_impulse(-collision.normal * (inertia * (sprint_speed if is_running else walk_speed)))
	
	
	
	
func directionProcessWithInput(direction_vector):
	var right = Input.is_action_pressed("game_right")
	var left  = Input.is_action_pressed("game_left")
	var up = Input.is_action_pressed("game_up")
	var down = Input.is_action_pressed("game_down")
	
	if right or left or up or down:
		state = MovementState.MOVING
	else:
		state = MovementState.IDLE
		if state == MovementState.IDLE:
			direction_vector.x = 0
			direction_vector.y = 0
		
	if state == MovementState.MOVING:
		if right:
			direction_vector.x = 1
		if left:
			direction_vector.x = -1
		if up:
			direction_vector.y = -1
		if down:
			direction_vector.y = 1
		direction_vector = direction_vector.normalized()
	return direction_vector


func _on_trunk_enter(tree):
	if Input.is_action_just_pressed("game_interact"):
		tree_to_cut = tree
		sprint_speed = walk_speed
		$Sprite/AnimationPlayer.play("Interact")


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Interact":
		sprint_speed = 200
