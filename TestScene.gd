extends Node

export(PackedScene) var Log

onready var player = $Map/Ysort/Player
onready var timer = $Timer
onready var day_night_cycle = $CanvasModulate
onready var timer_text = $CanvasLayer/TimerText
onready var map_y_sort = $Map/Ysort
onready var camera = $Camera2D
onready var main_menu = $CanvasLayer/MainMenu
var time_max = 0
var asking_for_reset = false

func _ready():
	GameEvents.connect("trunk_animation_finished", self, "_spawn_log")
	GameEvents.connect("player_wins", self, "win")
	day_night_cycle.color = Color(1.0, 1.0, 1.0, 1.0)
	print_timer()
	player.hide()

func _process(delta):
	#if(Input.is_key_pressed(KEY_R)):
		#reset()
	if asking_for_reset and Input.is_action_just_pressed("ui_accept"):
		reset()
	var multiplier = timer.time_left / time_max if time_max != 0 else 1
	multiplier = clamp(multiplier, 0.25, 0.75)
	day_night_cycle.color = Color(multiplier, multiplier, multiplier, 1.0)
	print_timer()

func calculate_seconds_int(time):
	return int(fmod(time, 60))

func calculate_minutes_int(time):
	return int(time/60)

func print_timer():
	timer_text.text = str(calculate_minutes_int(timer.time_left)) + ":%02d" % calculate_seconds_int(timer.time_left)

func _spawn_log(trunk_position):
	var wood_log = Log.instance()
	map_y_sort.add_child(wood_log)
	wood_log.set_global_position(trunk_position + Vector2(25, 0))
	wood_log.add_to_group("Pushable")

func _on_MainMenu_sign_text():
	camera.set_new_target($Map/SignPosition)

func reset():
	asking_for_reset = false
	$CanvasLayer/Label.hide()
	timer.stop()
	timer.wait_time = 0
	time_max = 0
	print_timer()
	player.is_alive = false
	player.hide()
	camera.reset()
	main_menu.reset()
	main_menu.show()
	$Map/Bridge.reset()
	for node in map_y_sort.get_children():
		if node.filename.ends_with("Tree.tscn"):
			node.reset()
	for node in map_y_sort.get_children():
		if node.filename.ends_with("Log.tscn"):
			node.queue_free()
	player.position.x = 187.514
	player.position.y = 32.214

func _on_MainMenu_start_game(difficulty):
	main_menu.stop()
	player.show()
	camera.set_new_target(player)
	timer.wait_time = difficulty
	yield(get_tree().create_timer(0.25), "timeout")
	$CanvasLayer/Label.text = "START"
	$CanvasLayer/Label.show()
	player.is_alive = true
	time_max = timer.wait_time
	timer.start()
	yield(get_tree().create_timer(0.5), "timeout")
	$CanvasLayer/Label.hide()

func win():
	player.is_alive = false
	$CanvasLayer/Label.text = "YOU SURVIVED!"
	$CanvasLayer/Label.show()
	yield(get_tree().create_timer(3), "timeout")
	$CanvasLayer/Label.text = "RESET?"
	asking_for_reset = true
	

func _on_Timer_timeout():
	player.is_alive = false
	$CanvasLayer/Label.text = "YOU'RE DEAD"
	$CanvasLayer/Label.show()
	yield(get_tree().create_timer(3), "timeout")
	$CanvasLayer/Label.text = "RESET?"
	asking_for_reset = true
