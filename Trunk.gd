extends AnimatedSprite

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimationPlayer.stop()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
	
	


func _on_AnimationPlayer_animation_finished(anim_name):
	GameEvents.emit_signal("trunk_animation_finished", get_global_transform().get_origin())
	
func cut_trunk():
	GameEvents.emit_signal("trunk_on_floor", Vector2(25,25), 5)
